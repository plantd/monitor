#include "application.h"
#include "window.h"
#include "sink.h"

struct _PlantEventApplication
{
  DzlApplication parent_instance;
};

G_DEFINE_TYPE (PlantEventApplication, plant_event_application, DZL_TYPE_APPLICATION)

static void
plant_event_application_activate (GApplication *app)
{
  GtkWindow *window;
  PlantEventApplication *self;

  self = PLANT_EVENT_APPLICATION (app);
  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (PLANT_TYPE_EVENT_WINDOW,
                           "application", app,
                           "default-width", 800,
                           "default-height", 600,
                           NULL);

  gtk_window_present (window);
}

static void
plant_event_application_shutdown (GApplication *app)
{
  PlantEventApplication *self;

  self = PLANT_EVENT_APPLICATION (app);
}

static void
plant_event_application_class_init (PlantEventApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = plant_event_application_activate;
  app_class->shutdown = plant_event_application_shutdown;
}

static void
about_activate (GSimpleAction *action,
                GVariant      *variant,
                gpointer       user_data)
{
  GtkAboutDialog *dialog;

  dialog = g_object_new (GTK_TYPE_ABOUT_DIALOG,
                         "copyright", "Copyright 2019 Geoff Johnson",
                         "logo-icon-name", "org.plantd.event",
                         "website", "https://gitlab.com/plantd/event",
                         "version", PLANTD_VERSION_S,
                         NULL);

  gtk_window_present (GTK_WINDOW (dialog));
}

static void
quit_activate (GSimpleAction *action,
               GVariant      *variant,
               gpointer       user_data)
{
  g_application_quit (G_APPLICATION (user_data));
}

static void
shortcuts_activate (GSimpleAction *action,
                    GVariant      *variant,
                    gpointer       user_data)
{
  DzlShortcutsWindow *window;
  DzlShortcutManager *manager;

  manager = dzl_application_get_shortcut_manager (user_data);

  window = g_object_new (DZL_TYPE_SHORTCUTS_WINDOW, NULL);
  dzl_shortcut_manager_add_shortcuts_to_window (manager, window);
  gtk_window_present (GTK_WINDOW (window));
}

static void
plant_event_application_init (PlantEventApplication *self)
{
  static GActionEntry entries[] = {
    { "about", about_activate },
    { "quit", quit_activate },
    { "shortcuts", shortcuts_activate },
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self), entries, G_N_ELEMENTS (entries), self);
}
