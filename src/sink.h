#pragma once

#include <apex/apex.h>

G_BEGIN_DECLS

#define PLANT_TYPE_EVENT_SINK (plant_event_sink_get_type())

G_DECLARE_FINAL_TYPE (PlantEventSink, plant_event_sink, PLANT, EVENT_SINK, ApexSink)

ApexSink *plant_event_sink_new (const gchar *endpoint, const gchar *filter);

G_END_DECLS
