#include "sink.h"

struct _PlantEventSink
{
  ApexSink parent_instance;
};

enum {
  EVENT,
  LAST_SIGNAL
};

G_DEFINE_TYPE (PlantEventSink, plant_event_sink, APEX_TYPE_SINK)

static guint signals[LAST_SIGNAL];

static void
plant_event_sink_handle_message (ApexSink    *self,
                                 const gchar *msg)
{
  g_autoptr (ApexEvent) event = NULL;

  event = apex_event_new ();
  apex_event_deserialize (event, msg);
  g_signal_emit (self, signals[EVENT], 0, event);
}

static void
plant_event_sink_finalize (GObject *object)
{
  G_OBJECT_CLASS (plant_event_sink_parent_class)->finalize (object);
}

static void
plant_event_sink_class_init (PlantEventSinkClass *klass)
{
  G_OBJECT_CLASS (klass)->finalize = plant_event_sink_finalize;
  APEX_SINK_CLASS (klass)->handle_message = plant_event_sink_handle_message;

  signals[EVENT] = g_signal_new ("event",
                                 G_TYPE_FROM_CLASS (klass),
                                 G_SIGNAL_RUN_LAST,
                                 0,
                                 NULL,
                                 NULL,
                                 NULL,
                                 G_TYPE_NONE,
                                 1,
                                 APEX_TYPE_EVENT);
}

static void
plant_event_sink_init (PlantEventSink *self)
{
}

ApexSink *
plant_event_sink_new (const gchar *endpoint,
                      const gchar *filter)
{
  return g_object_new (PLANT_TYPE_EVENT_SINK,
                       "endpoint", endpoint,
                       "filter", filter,
                       NULL);
}

