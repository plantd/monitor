#pragma once

#include <dazzle.h>

G_BEGIN_DECLS

// put this here for now
#define PLANTD_VERSION_S "0.1.0"

#define PLANT_TYPE_EVENT_APPLICATION (plant_event_application_get_type())

G_DECLARE_FINAL_TYPE (PlantEventApplication, plant_event_application, PLANT, EVENT_APPLICATION, DzlApplication)

G_END_DECLS
