#pragma once

#include <dazzle.h>

G_BEGIN_DECLS

#define PLANT_TYPE_EVENT_WINDOW (plant_event_window_get_type())

G_DECLARE_FINAL_TYPE (PlantEventWindow, plant_event_window, PLANT, EVENT_WINDOW, DzlApplicationWindow)

GtkWidget *plant_event_window_new (void);

G_END_DECLS
