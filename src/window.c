#include <glib/gi18n.h>

#include "window.h"
#include "sink.h"

struct _PlantEventWindow
{
  DzlApplicationWindow  parent_instance;
  GtkHeaderBar         *header_bar;
  GtkStack             *stack;
  GtkEntry             *entry_frontend;
  GtkEntry             *entry_backend;
  GtkToggleButton      *btn_connect;
  GtkSpinButton        *btn_id;
  GtkEntry             *entry_name;
  GtkEntry             *entry_description;
  GtkButton            *btn_send;
  GtkListStore         *liststore_events;
  GtkTreeView          *treeview_events;
  ApexSink             *sink;
  ApexSource           *source;
};

G_DEFINE_TYPE (PlantEventWindow, plant_event_window, DZL_TYPE_APPLICATION_WINDOW)

static const DzlShortcutEntry shortcuts[] = {
  {
    "org.event.window.Fullscreen", 0, "F11",
    N_("Editing"),
    N_("General"),
    N_("Fullscreen"),
    N_("Toggle window fullscreen")
  },
};

static void
plant_event_sink_event_cb (GObject   *object,
                           ApexEvent *event,
                           gpointer   user_data)
{
  PlantEventWindow *self;
  GtkTreeIter iter;
  g_autofree gchar *name;
  g_autofree gchar *desc;

  self = PLANT_EVENT_WINDOW (user_data);

  g_assert (APEX_IS_EVENT (event));
  g_assert (PLANT_IS_EVENT_WINDOW (self));

  name = g_strdup (apex_event_get_name (event));
  desc = g_strdup (apex_event_get_description (event));

  gtk_list_store_append (self->liststore_events, &iter);
  gtk_list_store_set (self->liststore_events, &iter,
                      0, apex_event_get_id (event),
                      1, apex_event_get_name (event),
                      2, apex_event_get_description (event),
                      -1);
  gtk_tree_view_set_model (GTK_TREE_VIEW (self->treeview_events),
                            GTK_TREE_MODEL (self->liststore_events));
}

static void
btn_connect_toggled_cb (GtkToggleButton *button,
                        gpointer        *user_data)
{
  PlantEventWindow *self;

  self = PLANT_EVENT_WINDOW (user_data);

  if (gtk_toggle_button_get_active (button))
    {
      g_debug ("Connect to backend: %s", gtk_entry_get_text (self->entry_backend));
      g_debug ("Connect to frontend: %s", gtk_entry_get_text (self->entry_frontend));

      apex_sink_set_endpoint (APEX_SINK (self->sink),
                              gtk_entry_get_text (self->entry_backend));
      apex_sink_start (APEX_SINK (self->sink));
      apex_source_set_endpoint (self->source,
                                gtk_entry_get_text (self->entry_frontend));
      apex_source_start (self->source);
    }
  else
    {
      apex_sink_stop (APEX_SINK (self->sink));
      apex_source_stop (self->source);
    }
}

static void
btn_send_clicked_cb (GtkButton *button,
                     gpointer   user_data)
{
  PlantEventWindow *self;
  g_autoptr (ApexEvent) event = NULL;
  g_autofree gchar *msg;

  self = PLANT_EVENT_WINDOW (user_data);

  event = apex_event_new_full (gtk_spin_button_get_value (self->btn_id),
                               gtk_entry_get_text (self->entry_name),
                               gtk_entry_get_text (self->entry_description));

  msg = apex_event_serialize (event);
  apex_source_queue_message (self->source, msg);
}

static void
plant_event_window_class_init (PlantEventWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/plantd/event/ui/window.ui");
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, stack);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, entry_frontend);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, entry_backend);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, btn_connect);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, liststore_events);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, treeview_events);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, btn_id);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, entry_name);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, entry_description);
  gtk_widget_class_bind_template_child (widget_class, PlantEventWindow, btn_send);

  gtk_widget_class_bind_template_callback (widget_class, btn_connect_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, btn_send_clicked_cb);
}

static void
plant_event_window_init (PlantEventWindow *self)
{
  DzlShortcutController *controller;

  gtk_widget_init_template (GTK_WIDGET (self));

  dzl_shortcut_manager_add_shortcut_entries (NULL, shortcuts, G_N_ELEMENTS (shortcuts), NULL);
  controller = dzl_shortcut_controller_find (GTK_WIDGET (self));
  dzl_shortcut_controller_add_command_action (controller,
                                              "org.event.window.Fullscreen",
                                              NULL,
                                              0,
                                              "win.fullscreen");

  g_signal_connect_swapped (self,
                            "key-press-event",
                            G_CALLBACK (dzl_shortcut_manager_handle_event),
                            dzl_shortcut_manager_get_default ());

  self->source = apex_source_new ("tcp://localhost:11001", "");
  self->sink = plant_event_sink_new ("tcp://localhost:12000", "");
  g_signal_connect (G_OBJECT (self->sink), "event",
                    (GCallback)plant_event_sink_event_cb,
                    self);

}

GtkWidget *
plant_event_window_new (void)
{
  return g_object_new (PLANT_TYPE_EVENT_WINDOW, NULL);
}
