#include "application.h"

gint
main (gint argc,
      gchar *argv[])
{
  g_autoptr(PlantEventApplication) app = NULL;
  gint ret;

  app = g_object_new (PLANT_TYPE_EVENT_APPLICATION,
                      "application-id", "org.plantd.Event",
                      "resource-base-path", "/org/plantd/event",
                      NULL);
  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}
